# /bin/sh

PARTICLES=100
SIM_TIME=400
FILE=~/Desktop/msc-thesis/swarmalators/scan.log

cd build/bin/&&
for K in -0.8 -0.4 0.0 0.4
do
  for J in -0.4 0.0 0.4 0.8
  do
    ./Swarmalators $PARTICLES $J $K $SIM_TIME | tee -a $FILE 
  done
done
