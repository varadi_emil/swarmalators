#! /bin/sh


PARTICLES=100
SIM_TIME=150
FILE=~/Desktop/msc-thesis/swarmalators/map_log.log
TYPE=default
ENABLE_GRAPHICS=false

cd build/bin/ &&
for K in -0.8 -0.7 -0.6 -0.5 -0.4 -0.3 -0.2 -0.1 0.0 0.1 0.2
do
    for J in -0.8 -0.7 -0.6 -0.5 -0.4 -0.3 -0.2 -0.1 0.0 0.1 0.2
    do
        ./Swarmalators $PARTICLES $J $K $SIM_TIME $TYPE $ENABLE_GRAPHICS | tee -a $FILE
    done
done
