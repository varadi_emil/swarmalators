#include "solver_controller.h"

#include <iostream>
#include <thread>

SolverController::SolverController() {}

void SolverController::addSolver(AdamsBashforthSolver& solver) {
    this->solvers.push_back(solver);
}

void SolverController::step() {
    for(int i = 0; i < solvers.size(); i++) {
        threads.push_back(std::thread(&AdamsBashforthSolver::step, &solvers.at(i)));
    }

    for(int i = 0; i < threads.size(); i++) {
        threads.at(i).join();
    }

    for(int i = 0; i < threads.size(); i++) {
        solvers.at(i).commit();
    }

    threads.clear();
}