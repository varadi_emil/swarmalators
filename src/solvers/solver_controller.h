#pragma once

#include<vector>
#include<thread>

#include "adams_bashforth_solver.h"

class SolverController {
private:
    std::vector<AdamsBashforthSolver> solvers;
    std::vector<std::thread> threads;
public:
    SolverController();
    void addSolver(AdamsBashforthSolver&);
    void step();
};
