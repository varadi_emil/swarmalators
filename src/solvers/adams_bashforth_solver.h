#pragma once

#include "../models/swarmalator.h"

#include <functional>
class AdamsBashforthSolver {
private:
    double coefficients[4] = {55.0/24, -59.0/24, 37.0/24, -9.0/24};
    double derivatives[4] = {0.0, 0.0, 0.0, 0.0};
    double &x;
    double newValue;
    double dt;
    std::function<double()> derivativeGetter;

    double dotProduct();
    void enqueueDerivative(double);
public:
    AdamsBashforthSolver(double&, double, std::function<double()>);
    void step();
    void commit();
};