#include "adams_bashforth_solver.h"

#include<iostream>

AdamsBashforthSolver::AdamsBashforthSolver(double& value, double dt, std::function<double()> derivativeGetter):x(value) {
    this->dt = dt;
    this->derivativeGetter = derivativeGetter;
}

void AdamsBashforthSolver::enqueueDerivative(double newDerivative) {
    for(int i = 3; i > 0; i--) {
        derivatives[i] = derivatives[i-1];
    }
    derivatives[0] = newDerivative;
}

double AdamsBashforthSolver::dotProduct() {
    return coefficients[0]*derivatives[0] + coefficients[1]*derivatives[1] + coefficients[2]*derivatives[2] + coefficients[3]*derivatives[3];
}

void AdamsBashforthSolver::step() {
    enqueueDerivative(derivativeGetter());

    newValue = x + dotProduct()*dt;
    x = newValue;
}

void AdamsBashforthSolver::commit() {
    x = newValue;
}