#include "graphics.h"

#include <cmath>

Graphics::Graphics(sf::RenderWindow& renderWindow, std::vector<Swarmalator>& swarmalators): window(renderWindow), entities(swarmalators) {
    windowWidth = window.getSize().x*0.8;
    windowHeight = window.getSize().y*0.8;
    horizontalPadding = window.getSize().x*0.2;
    verticalPadding = window.getSize().y*0.2;

    for(int i = 0; i < entities.size(); i++) {
        shapes.push_back(sf::CircleShape(5.f));
        shapes.at(i).setFillColor(sf::Color::Green);
    }
}

void Graphics::putEntitiesToScreen() {
    for(int i = 0; i < entities.size(); i++) {
        shapes.at(i).setPosition(mapCoordinatesToScreen(entities.at(i).x, entities.at(i).y));
        shapes.at(i).setFillColor(mapPhaseToColor(entities.at(i).phase));
        window.draw(shapes.at(i));
    }
}

sf::Vector2f Graphics::mapCoordinatesToScreen(double x, double y) {
    return sf::Vector2f(x*windowWidth/4 + windowWidth/2 + horizontalPadding/2, y*windowHeight/4 + windowHeight/2 + verticalPadding/2);
}

sf::Color Graphics::mapPhaseToColor(double phase) {
    phase = fmod(phase, 2.0*M_PI);
    while(phase < 0.0) {
        phase += 2.0*M_PI;
    }

    return hsv((int) phase*180/M_PI, 1.0, 1.0);
}

/// Source: https://en.sfml-dev.org/forums/index.php?topic=7313.0
sf::Color Graphics::hsv(int hue, float sat, float val) {
  hue %= 360;
  while(hue<0) hue += 360;

  if(sat<0.f) sat = 0.f;
  if(sat>1.f) sat = 1.f;

  if(val<0.f) val = 0.f;
  if(val>1.f) val = 1.f;

  int h = hue/60;
  float f = float(hue)/60-h;
  float p = val*(1.f-sat);
  float q = val*(1.f-sat*f);
  float t = val*(1.f-sat*(1-f));

  switch(h)
  {
    default:
    case 0:
    case 6: return sf::Color(val*255, t*255, p*255);
    case 1: return sf::Color(q*255, val*255, p*255);
    case 2: return sf::Color(p*255, val*255, t*255);
    case 3: return sf::Color(p*255, q*255, val*255);
    case 4: return sf::Color(t*255, p*255, val*255);
    case 5: return sf::Color(val*255, p*255, q*255);
  }
}