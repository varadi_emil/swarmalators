#pragma once

#include<vector>

#include<SFML/Graphics.hpp>

#include"../models/swarmalator.h"

class Graphics {
private:
    sf::RenderWindow& window;
    int windowHeight;
    int windowWidth;
    int horizontalPadding;
    int verticalPadding;
    std::vector<Swarmalator>& entities;
    std::vector<sf::CircleShape> shapes;

    sf::Vector2f mapCoordinatesToScreen(double, double);
    sf::Color mapPhaseToColor(double);
    sf::Color hsv(int, float, float);
public:
    Graphics(sf::RenderWindow&, std::vector<Swarmalator>&);
    void putEntitiesToScreen();
};