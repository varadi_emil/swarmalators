#include "velocity_tracker.h"

#include <cmath>
#include <iostream>

VelocityTracker::VelocityTracker(std::vector<Swarmalator>& swarmalators, double dt): swarmalators(swarmalators), dt(dt) {
    numberOfSamples = 0.0;
}

void VelocityTracker::track() {
    double vxi;
    double vyi;

    double newVx = 0.0;
    double newVy = 0.0;
    double newV2 = 0.0;

    for(int i = 0; i < swarmalators.size(); i++) {
        vxi = (xCache.at(i) - swarmalators.at(i).x)/dt;
        vyi = (yCache.at(i) - swarmalators.at(i).y)/dt;

        newVx += vxi;
        newVy += vyi;
        newV2 += (pow(vxi, 2) + pow(vyi, 2));  
    }

    newVx /= (double) swarmalators.size();
    newVy /= (double) swarmalators.size();
    newV2 /= (double) swarmalators.size();

    numberOfSamples += 1.0;
    vx = (vx*(numberOfSamples - 1.0) + newVx)/numberOfSamples;
    vy = (vy*(numberOfSamples - 1.0) + newVy)/numberOfSamples;
    v2 = (v2*(numberOfSamples - 1.0) + newV2)/numberOfSamples;

    fillCache();
}

void VelocityTracker::fillCache() {
    xCache.clear();
    yCache.clear();

    for(int i = 0; i < swarmalators.size(); i++) {
        xCache.push_back(swarmalators.at(i).x);        
        yCache.push_back(swarmalators.at(i).y);        
    }
}

double VelocityTracker::getVxAverage() {
    return vx;
}

double VelocityTracker::getVyAverage() {
    return vy;
}

double VelocityTracker::getV2Average() {
    return v2;
}