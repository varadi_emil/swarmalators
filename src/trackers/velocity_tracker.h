#pragma once

#include<vector>

#include "../contracts/tracker.h"
#include "../models/swarmalator.h"

class VelocityTracker: public Tracker {
private:
    std::vector<Swarmalator>& swarmalators;
    
    std::vector<double> xCache;
    std::vector<double> yCache;

    double numberOfSamples;
    double dt;
    double vx;
    double vy;
    double v2;

public:
    VelocityTracker(std::vector<Swarmalator>&, double);
    void track();
    void fillCache();

    double getVxAverage();
    double getVyAverage();
    double getV2Average();
};
