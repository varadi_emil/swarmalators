#pragma once

#include <vector>

#include "../contracts/tracker.h"
#include "../models/swarmalator.h"

class KuramotoOrderParameterTracker: public Tracker {
private:
    std::vector<Swarmalator>& swarmalators;

    double orderParameter;
    double numberOfSamples;
public:
    KuramotoOrderParameterTracker(std::vector<Swarmalator>&);
    void track();
    double getAverage();
};
