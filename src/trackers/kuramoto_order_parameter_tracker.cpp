#include "kuramoto_order_parameter_tracker.h"

#include<cmath>
#include<iostream>

KuramotoOrderParameterTracker::KuramotoOrderParameterTracker(std::vector<Swarmalator>& swarmalators): swarmalators(swarmalators) {
    orderParameter = 0.0;
    numberOfSamples = 1.0;
}

void KuramotoOrderParameterTracker::track() {
    double cosAverage = 0.0;
    double sinAverage = 0.0;

    for(int i = 0; i < swarmalators.size(); i++) {
        cosAverage += cos(swarmalators.at(i).phase);
        sinAverage += sin(swarmalators.at(i).phase);
    }

    cosAverage /= (double) swarmalators.size();
    sinAverage /= (double) swarmalators.size();
    
    double newSample = sqrt(pow(cosAverage, 2) + pow(sinAverage, 2));
    numberOfSamples += 1.0;
    orderParameter = (orderParameter * (numberOfSamples - 1.0) + newSample)/numberOfSamples;
}

double KuramotoOrderParameterTracker::getAverage() {
    return orderParameter;
}