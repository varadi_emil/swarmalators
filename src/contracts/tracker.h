#pragma once

class Tracker {
public:
    virtual void track() = 0;
};