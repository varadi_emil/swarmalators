file(GLOB_RECURSE SRCS *.cpp)
file(GLOB_RECURSE HDRS *.h)

add_library(src SHARED
            ${SRCS}
            ${HDRS}
            )

target_include_directories(src
                         PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}"
                         )