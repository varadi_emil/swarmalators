#include "swarmalator_factory.h"

#include<cmath>

#include "../rng/rng.h"


Swarmalator SwarmalatorFactory::random() {
    Rng& rng = Rng::get();

    return Swarmalator(
        rng.getUniformDouble(-1.0, 1.0),
        rng.getUniformDouble(-1.0, 1.0),
        rng.getUniformDouble(0.0, 2.0*M_PI),
        rng.getUniformDouble(2.0*M_PI - 2.0*M_PI/100.0, 2.0*M_PI + 2.0*M_PI/100.0)
    );
}

Swarmalator SwarmalatorFactory::constFreqency() {
    Rng& rng = Rng::get();

    return Swarmalator(
        rng.getUniformDouble(-1.0, 1.0),
        rng.getUniformDouble(-1.0, 1.0),
        rng.getUniformDouble(0.0, 6.28),
        0.0
    );
}