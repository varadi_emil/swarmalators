#pragma once

#include "../models/swarmalator.h"

class SwarmalatorFactory {
public:
    static Swarmalator random();
    static Swarmalator constFreqency();
};