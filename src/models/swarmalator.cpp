#include"swarmalator.h"

#include<iostream>
#include<cmath>

Swarmalator::Swarmalator(double x, double y, double phase, double frequency) {
    this->x = x;
    this->y = y;
    this->phase = phase;
    this->frequency = frequency;
}

double Swarmalator::getDistanceTo(Swarmalator* other) {
    return sqrt(pow(this->x - other->x, 2) + pow(this->y - other->y, 2));
}

std::string Swarmalator::toString() {
    std::string stringRepresentation = std::to_string(this->x) + " " + std::to_string(this->y) + " " + std::to_string(this->phase) + " " + std::to_string(this->frequency);

    return stringRepresentation;
}