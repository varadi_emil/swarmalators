#pragma once

#include<string>
#include<vector>

class Swarmalator {
public:
    double x;
    double y;
    double phase;
    double frequency;

    Swarmalator(double, double, double, double);
    double getDistanceTo(Swarmalator*);
    std::string toString();
};