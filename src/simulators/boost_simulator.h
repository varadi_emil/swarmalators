#pragma once

#include <vector>
#include <boost/array.hpp>

#include "../models/swarmalator.h"

#define NUMBER_OF_ENTITIES 10

typedef boost::array<double, NUMBER_OF_ENTITIES*3> state_type;

class BoostSimulator {
private:
    std::vector<Swarmalator> swarmalators;
    static void system(const state_type&, state_type&, double);
    static void systemLogger(const state_type&, const double);

public:

    BoostSimulator();
    void initialize();
    void run();
};