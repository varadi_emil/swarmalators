#pragma once

#include <vector>

#include "../models/swarmalator.h"
#include "swarmalator_type.h"

class Simulator {
private:
    std::vector<Swarmalator> swarmalators;

    double J = 0.1;
    double K = 1.0;
    double dt = 0.01;

    double getDerivativeX(int);
    double getDerivativeY(int);
    double getDerivativePhase(int);
public:
    Simulator(int, double, double, SwarmalatorType);
    void run(double = 1.0, bool = true);
};