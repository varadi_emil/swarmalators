#include "boost_simulator.h"

#include "../factories/swarmalator_factory.h"

#include <iostream>
#include <boost/array.hpp>

#include <boost/numeric/odeint.hpp>

using namespace std;
using namespace boost::numeric::odeint;

BoostSimulator::BoostSimulator() {}

void BoostSimulator::initialize() {
    for(int i = 0; i < NUMBER_OF_ENTITIES; i++) {
        this->swarmalators.push_back(SwarmalatorFactory::random());
    }
}

void BoostSimulator::run() {
    int numberOfVariables = NUMBER_OF_ENTITIES * 3;
    state_type initalConditions;

    for(int i = 0; i < NUMBER_OF_ENTITIES; i++) {
        initalConditions[i*3] = swarmalators.at(i).x; 
        initalConditions[i*3 + 1] = swarmalators.at(i).y;
        initalConditions[i*3 + 2] = swarmalators.at(i).phase;
    }

    integrate(system, initalConditions, 0.0, 2.0, 0.01, systemLogger);
}

void BoostSimulator::system(const state_type &variables, state_type &derivatives, double t) {
    for(int i = 0; i < NUMBER_OF_ENTITIES; i++) {
        derivatives[i*3] = variables[i*3];
        derivatives[i*3 + 1] = variables[i*3 + 1];
        derivatives[i*3 + 2] = variables[i*3 + 2];
    }
}

void BoostSimulator::systemLogger(const state_type &state, const double t) {
    for(int i = 0; i < NUMBER_OF_ENTITIES*3; i++) {
        cout << state[i] << " ";
    }
    cout << t << std::endl << std::endl;
}