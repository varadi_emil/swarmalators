#include "simulator.h"

#include<iostream>
#include<cmath>

#include<SFML/Graphics.hpp>

#include "../factories/swarmalator_factory.h"
#include "../solvers/solver_controller.h"
#include "../graphics/graphics.h"
#include "../contracts/tracker.h"
#include "../trackers/kuramoto_order_parameter_tracker.h"
#include "../trackers/velocity_tracker.h"

Simulator::Simulator(int numberOfEntities, double J, double K, SwarmalatorType type) {
    this->J = J;
    this->K = K;

    for(int i = 0; i < numberOfEntities; i++) {
        switch (type) {
            case SwarmalatorType::constZero:
                this->swarmalators.push_back(SwarmalatorFactory::constFreqency());
                break;
            case SwarmalatorType::randomlyDistributed:
                this->swarmalators.push_back(SwarmalatorFactory::random());
                break;
            default:
                this->swarmalators.push_back(SwarmalatorFactory::constFreqency());
                break;
        }
    }
}

void Simulator::run(double finalTime, bool enableGraphics) {
    SolverController controller = SolverController();
    KuramotoOrderParameterTracker* kuramotoTracker = new KuramotoOrderParameterTracker(swarmalators);
    VelocityTracker* velocityTracker = new VelocityTracker(swarmalators, dt);

    for(int i = 0; i < swarmalators.size(); i++) {
        AdamsBashforthSolver xSolver(
            swarmalators.at(i).x, 
            dt,
            [=]() {return getDerivativeX(i);}
        );
        controller.addSolver(xSolver);

        AdamsBashforthSolver ySolver(
            swarmalators.at(i).y, 
            dt,
            [=]() {return getDerivativeY(i);}
        );
        controller.addSolver(ySolver);

        AdamsBashforthSolver phaseSolver(
            swarmalators.at(i).phase, 
            dt,
            [=]() {return getDerivativePhase(i);}
        );
        controller.addSolver(phaseSolver);
    } 

    double t = 0.0;

    if(enableGraphics) {
        sf::RenderWindow window(sf::VideoMode(600, 600), "Swarmalators");
        Graphics graphics = Graphics(window, swarmalators);

        while (window.isOpen())
        {
            if(t < finalTime) {
                controller.step();
                t += dt;

                if(t > finalTime/2.0) {
                    kuramotoTracker->track();
                    velocityTracker->track();
                }
                else if(t + dt > finalTime/2.0) {
                    velocityTracker->fillCache();
                }
            }
            else {
                window.close();
            }
            sf::Event event;
            while (window.pollEvent(event))
            {
                if (event.type == sf::Event::Closed)
                    window.close();
            }

            window.clear();
            graphics.putEntitiesToScreen();
            window.display();
        }

        std::cout << "J: " << J << std::endl;
        std::cout << "K: " << K << std::endl;
        std::cout << "Particles: " << swarmalators.size() << std::endl;
        std::cout << "Kuramoto order param: " << kuramotoTracker->getAverage() << std::endl;
        std::cout << "<vx>: " << velocityTracker->getVxAverage() << std::endl;
        std::cout << "<vy>: " <<velocityTracker->getVyAverage() << std::endl;
        std::cout << "<v^2>: " << velocityTracker->getV2Average() << std::endl;
        std::cout << std::endl;
        delete kuramotoTracker;
        delete velocityTracker;
        return;
    }

    while(t < finalTime) {
        controller.step();
        t += dt;

        if(t > finalTime/2.0) {
            kuramotoTracker->track();
            velocityTracker->track();
        }
        else if(t + dt > finalTime/2.0) {
            velocityTracker->fillCache();
        }
    }

    std::cout << J << " " << K << " " << swarmalators.size() << " " << kuramotoTracker->getAverage() << " " << velocityTracker->getVxAverage() << " " << velocityTracker->getVyAverage() << " " << velocityTracker->getV2Average() << std::endl;
}

/// UTILS
double Simulator::getDerivativeX(int i) {
    double derivative = 0.0;

    for(int j = 0; j < swarmalators.size(); j++) {
        if(i == j) {
            continue;
        }

        derivative += (swarmalators.at(j).x - swarmalators.at(i).x)/swarmalators.at(i).getDistanceTo(&swarmalators.at(j)) * (1.0 + J*cos(swarmalators.at(j).phase - swarmalators.at(i).phase)) - (swarmalators.at(j).x - swarmalators.at(i).x)/pow(swarmalators.at(i).getDistanceTo(&swarmalators.at(j)), 2); 
    }

    derivative/=(double) swarmalators.size();

    return derivative;
}

double Simulator::getDerivativeY(int i) {
    double derivative = 0.0;

    for(int j = 0; j < swarmalators.size(); j++) {
        if(i == j) {
            continue;
        }

        derivative += (swarmalators.at(j).y - swarmalators.at(i).y)/swarmalators.at(i).getDistanceTo(&swarmalators.at(j)) * (1.0 + J*cos(swarmalators.at(j).phase - swarmalators.at(i).phase)) - (swarmalators.at(j).y - swarmalators.at(i).y)/pow(swarmalators.at(i).getDistanceTo(&swarmalators.at(j)), 2); 
    }

    derivative/=(double) swarmalators.size();

    return derivative;
}

double Simulator::getDerivativePhase(int i) {
    double derivative = 0.0;

    for(int j = 0; j < swarmalators.size(); j++) {
        if(i == j) {
            continue;
        }

        derivative += swarmalators.at(i).frequency + K/swarmalators.size()*sin(swarmalators.at(j).phase - swarmalators.at(i).phase)/swarmalators.at(i).getDistanceTo(&swarmalators.at(j));
    }

    return derivative;
}
