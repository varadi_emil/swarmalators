#include "rng.h"

#include <stdio.h>

double Rng::getUniformDouble(double lowerLimit, double upperLimit) {
    double r = (double) rand()/((double) RAND_MAX + 1.0);
    return lowerLimit + r*(upperLimit - lowerLimit);
}