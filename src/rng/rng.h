#pragma once


#include <stdlib.h>
#include <time.h>

class Rng {
public:
    Rng(const Rng&) = delete;
    
    static Rng& get() {
        static Rng instance;

        return instance;
    }
    static double getUniformDouble(double = 0.0, double = 1.0);
private:
    Rng() {
        srand(time(NULL));
    }
};