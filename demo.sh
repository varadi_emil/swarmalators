#! /bin/sh

PARTICLES=100
SIM_TIME=400
FILE=~/Desktop/msc-thesis/swarmalators/demo_log.log

cd build/bin/&&
./Swarmalators $PARTICLES 0.1 1.0 $SIM_TIME | tee -a $FILE &&
./Swarmalators $PARTICLES 0.1 -1.0 $SIM_TIME | tee -a $FILE &&
./Swarmalators $PARTICLES 1.0 0.0 $SIM_TIME | tee -a $FILE &&
./Swarmalators $PARTICLES 1.0 -0.1 $SIM_TIME | tee -a $FILE &&
./Swarmalators $PARTICLES 1.0 -0.75 $SIM_TIME | tee -a $FILE 