#! /bin/sh


PARTICLES=100
SIM_TIME=400
FILE=~/Desktop/msc-thesis/swarmalators/nonzero_log.log
TYPE=randomlyDistributed

cd build/bin/ &&
./Swarmalators $PARTICLES -0.2 -0.4 $SIM_TIME $TYPE | tee -a $FILE
./Swarmalators $PARTICLES -0.6 0.2 $SIM_TIME $TYPE | tee -a $FILE
./Swarmalators $PARTICLES 0.6 -0.4 $SIM_TIME $TYPE | tee -a $FILE
./Swarmalators $PARTICLES 0.8 -0.6 $SIM_TIME $TYPE | tee -a $FILE
./Swarmalators $PARTICLES 0.8 -0.1 $SIM_TIME $TYPE | tee -a $FILE
./Swarmalators $PARTICLES 0.6 0.2 $SIM_TIME $TYPE | tee -a $FILE
./Swarmalators $PARTICLES -0.2 0.2 $SIM_TIME $TYPE | tee -a $FILE