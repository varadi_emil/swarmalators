#! /bin/sh

PARTICLES=100
SIM_TIME=400
FILE=~/Desktop/msc-thesis/swarmalators/j08_log.log
J=0.8

cd build/bin/&&

for K in -1.0 -0.8 -0.6 -0.4 -0.2 0.0 0.2
do
    ./Swarmalators $PARTICLES $J $K $SIM_TIME | tee -a $FILE    
done