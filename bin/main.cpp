#include "simulators/simulator.h"
#include "simulators/swarmalator_type.h"

#include <string>

int main(int argc, char* argv[]) {
  if(argc < 5) {
    printf("Insufficient arguments!");
    return 1;
  }

  int entities = atoi(argv[1]);
  double J = atof(argv[2]);
  double K = atof(argv[3]);
  double finalTime = atof(argv[4]);
  std::string typeArgument;
  if(argc >= 6) {
    typeArgument = argv[5];
  }

  bool enableGraphics = true;
  if(argc >= 7) {
    std::string argument = argv[6];
    if(argument == "false") {
      enableGraphics = false;
    }
  }

  SwarmalatorType type = SwarmalatorType::constZero;
  if(typeArgument == "randomlyDistributed") {
    type = SwarmalatorType::randomlyDistributed;
  } 

  Simulator mySimulator = Simulator(entities, J, K, type);
  mySimulator.run(finalTime, enableGraphics);

  return 0;
}
